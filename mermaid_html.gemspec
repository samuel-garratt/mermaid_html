
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mermaid_html/version'

Gem::Specification.new do |spec|
  spec.name          = 'mermaid_html'
  spec.version       = MermaidHtml::VERSION
  spec.authors       = ['Samuel Garratt']
  spec.email         = ['samuel.garratt@integrationqa.com']

  spec.summary       = %q{Helps to convert a Mermaid script into a html page showing the diagram}
  spec.description   = %q{Helps to convert a Mermaid script into a html page showing the diagram.}
  spec.homepage      = 'https://gitlab.com/samuel-garratt/mermaid_html'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'thor'
  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'page-object'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
end
