#!/usr/bin/env ruby

require 'thor'
$LOAD_PATH.unshift File.join(File.dirname(__FILE__), '..', 'lib')
require 'mermaid_html'
require 'fileutils'

# Create folder if there's not a file already there.
# Will create parent folder if necessary.
# @param [String] folder Folder to create
def create_folder(folder)
  if File.exist? folder
    unless File.directory? folder
      warn "!! #{folder} already exists and is not a directory"
    end
  else
    FileUtils.mkdir_p folder
    puts "Created folder: #{folder}/"
  end
end

# @param [String] filename Name of the file to create
# @param [String] content Content to place inside file
def create_file(filename: nil, content: nil)
  raise 'Need to pass filename' unless filename
  raise 'Need to pass contents to insert into file' unless content
  create_folder File.split(filename).first
  if File.exist? filename
    old_content = File.read(filename)
    puts "Updated #{filename}" if old_content != content
  else
    puts 'Created: ' + filename
  end
  File.open(filename, 'w') { |f| f.puts content }
end

class Exe < Thor

  include MermaidHtml::MermaidFiles

  option :css, default: 'css', banner: 'What folder to put css in'
  option :js, default: 'js', banner: 'What folder to put js in'
  option :out, default: '.', banner: 'Output folder relative to current'
  desc 'file [file_name]', 'Create Mermaid html from schema file'
  def file(filename)
    MermaidHtml.css_folder = options[:css]
    MermaidHtml.js_folder = options[:js]
    create_file filename: File.join(options[:out], MermaidHtml.css_folder, 'mermaid.css'), content: mermaid_css
    create_file filename: File.join(options[:out], MermaidHtml.css_folder, 'mermaid-font.css'), content: mermaid_font
    create_file filename: File.join(options[:out], MermaidHtml.js_folder, 'mermaid.js'), content: mermaid_js
    create_file filename: File.join(options[:out], "#{filename}.html"), content: mermaid_html(File.read(filename))
  end
end

Exe.start(ARGV)
