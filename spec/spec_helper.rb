require 'rspec'
require 'bundler/setup'
require 'mermaid_html'
require 'page-object'

RSpec.configure do |config|
  config.before(:all) do
    @browser = Watir::Browser.new :chrome, url: "http://chrome:4444/wd/hub"
  end
  config.include PageObject::PageFactory

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!
end
