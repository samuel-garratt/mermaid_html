# frozen_string_literal: true

require_relative 'test_diagram_page'

RSpec.describe MermaidHtml do
  it 'diagram looks correct' do
    visit(TestDiagramPage) do |page|
      expect(page.nodes.count).to eq 4
      first_node_size = page.nodes.first.size
      expect(first_node_size.width).to be > 29
      expect(first_node_size.height).to be > 35
    end
  end
end
