require 'page-object'

class TestDiagramPage
  include PageObject

  def current_location
    File.join(FileUtils.pwd.tr('/', '\\'), 'spec')
  end

  page_url 'file://<%= current_location %>\test_diagram.html'

  def nodes
    browser.elements(class: 'node')
  end
end
