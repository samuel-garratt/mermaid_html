require 'erb'

module MermaidHtml
  # Reads main files for mermaidjs
  module MermaidFiles

    # @return [String] HTML for displaying MermaidJS diagram
    def mermaid_html(schema)
      @schema = schema
      ERB.new(relative_file('mermaid_template.html.erb')).result(binding)
    end

    # Main css for Mermaidjs
    def mermaid_css
      relative_file 'mermaid.css'
    end

    # Font css for Mermaidjs
    def mermaid_font
      relative_file 'mermaid-font.css'
    end

    # Main js for Mermaidjs
    def mermaid_js
      relative_file 'mermaid.js'
    end

    # Read file relative to current directory
    def relative_file(file)
      File.read(File.join(File.dirname(__FILE__), file))
    end
  end
end