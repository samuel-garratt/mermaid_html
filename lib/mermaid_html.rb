require 'mermaid_html/version'
require 'mermaid_html/mermaid_files'

module MermaidHtml
  @css_folder = 'css'
  @js_folder = 'js'
  class << self
    # Folder css will be added to
    attr_accessor :css_folder
    # Folder js will be added to
    attr_accessor :js_folder
  end
end
